
import $ from "jquery";
import WOW from "wowjs";

$(document).ready(() => {
    let wow = new WOW.WOW(
        {
            boxClass: "wow",      // animated element css class (default is wow)
            animateClass: "animated", // animation css class (default is animated)
            offset: 10,          // distance to the element when triggering the animation (default is 0)
            mobile: true,       // trigger animations on mobile devices (default is true)
            live: true,       // act on asynchronously loaded content (default is true)
            callback: function () {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: "tr",    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
    wow.sync();
});
class Menu {
    constructor(menu, open) {
        this.menu = $(menu);
        this.menuOpenNode = $(open);
        this.menuCloseNode = this.menu.find(".js-menu-close");
        this.menuOpenNode.on("click", () => this.menuOpen());
        this.menuCloseNode.on("click", () => this.menuClose());
    }

    menuOpen() {
        this.menu.addClass("open");
    }

    menuClose() {
        this.menu.removeClass("open");
    }
}
const MenuModule = new Menu(".js-menu", ".js-menu-open");


$(".page-2__input").each((i, item) => {

    $(item).on("focus", ({target}) => {
        let node = $(target);       
        node.closest(".page-2__label").addClass("not-empty");
    });

    $(item).on("blur", ({target}) => {
        let node = $(target);
        if (node.val() !== "") {
            node.closest(".page-2__label").addClass("not-empty");
        } else {
            node.closest(".page-2__label").removeClass("not-empty");
        }
    });
});

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);
    node.classList.add("animated", animationName);

    function handleAnimationEnd() {
        node.classList.remove("animated", animationName);
        node.removeEventListener("animationend", handleAnimationEnd);

        if (typeof callback === "function") callback();
    }
    node.addEventListener("animationend", handleAnimationEnd);
}


export { animateCSS };
export default MenuModule;
