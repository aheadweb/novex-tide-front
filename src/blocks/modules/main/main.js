import $ from "jquery";
import MenuModule, { animateCSS } from "../header/header";


class Slider {
    constructor(body, nav) {
        this.body = $(body);
        this.nav = $(nav);
        this.nav.on("click", (e) => this.goToSlide(e));
        this.slides = this.body.find(".js-slide");
        this.maxSlide = this.slides.length - 1;
        this.lastSlideNode = this.slides[this.maxSlide];

        this.activeSlide = 0;
        this.lastActiveSlide = null;
        this.canSlide = true;

        this.firstLastScroll = true;
        this.init();

    }

    init() {
        this.setActiveSlide();
        $(window).on("wheel", (e) => this.onChangeSlide(e));
    }

    goToSlide({ target }) {
        let node = $(target);
        let toSlide = node.data("toSlide");
        if (toSlide !== undefined) {
            this.activeSlide = toSlide;
            MenuModule.menuClose();
            this.clearAllSlide(toSlide);
            $(window).off();
            this.firstLastScroll = true;
            this.lastActiveSlide = null;
            this.setActiveSlide();

            if(toSlide === 2) {
                $(".js-menu-open").addClass("blue");
            } else {
                $(".js-menu-open").removeClass("blue");
            }

            if (toSlide === this.maxSlide) {
                $(window).on("wheel", (e) => this.onLastSlideChekc(e));
            } else {
                $(window).on("wheel", (e) => this.onChangeSlide(e));
            }

        }
    }

    clearAllSlide(toSlide) {
        this.slides.each((i, slide) => {
            let node = $(slide);
            node.removeClass("active last wathed");
            if (i < toSlide) {
                node.addClass("wathed");
            }
            if (i === this.maxSlide) {
                node.addClass("last");
            }

        });
    }

    onLastSlideChekc() {
        if (this.canSlide) {
            let scroll = 10;

            if (!this.firstLastScroll) {
                scroll = this.lastSlideNode.scrollTop;
            }
            this.firstLastScroll = false;
            if (scroll === 0) {
                this.firstLastScroll = true;
                $(window).off("wheel");
                $(window).on("wheel", (e) => this.onChangeSlide(e));
            }
        }


    }

    onChangeSlide(e) {
        if (this.canSlide) {
            e.originalEvent.deltaY > 0
                ? this.slideNext()
                : this.slideBack();
        }
    }

    slideBack() {
        if (this.activeSlide <= 0) {
            return;
        }

        this.canSlide = false;

        setTimeout(() => {
            this.canSlide = true;
        }, 1300);

        this.lastActiveSlide = this.activeSlide;
        this.activeSlide = this.activeSlide - 1;

        if(this.activeSlide === 2) {
            $(".js-menu-open").addClass("blue");
        } else {
            $(".js-menu-open").removeClass("blue");
        }

        this.clearWathedSlide();
        this.setActiveSlide();
        this.clearLastActiveSlide();
    }


    slideNext() {

        if (this.activeSlide >= this.maxSlide) {
            return;
        }
        this.canSlide = false;

        setTimeout(() => {
            this.canSlide = true;
        }, 1300);

        this.lastActiveSlide = this.activeSlide;
        this.activeSlide = this.activeSlide + 1;

        if(this.activeSlide === 2) {
            $(".js-menu-open").addClass("blue");
            animateCSS(".page-2__img","fadeInUpBig");
        } else {
            $(".js-menu-open").removeClass("blue");
        }

        this.setActiveSlide();
        this.setWathedSlide();
        this.clearLastActiveSlide();

        if (this.activeSlide === this.maxSlide) {
            this.isLastSlide();
            $(".card-winner").each((i,item) => {
                $(item).css("animation-delay", `${i / 5}s`);
                $(item).addClass("animated fadeIn");
            });
        }

    }

    isLastSlide() {
        this.body.find(`[data-slide=${this.activeSlide}]`).addClass("last");
        $(window).off("wheel");
        $(window).on("wheel", (e) => this.onLastSlideChekc(e));
    }

    setWathedSlide() {
        this.body.find(`[data-slide=${this.lastActiveSlide}]`).addClass("wathed");
    }

    clearWathedSlide() {
        this.body.find(`[data-slide=${this.activeSlide}]`).removeClass("wathed");
    }

    setActiveSlide() {
        this.body.find(`[data-slide=${this.activeSlide}]`).addClass("active");
    }

    clearLastActiveSlide() {
        this.body.find(`[data-slide=${this.lastActiveSlide}]`).removeClass("active");
    }
}




if (window.innerWidt > 1024 || !isTouchDevice()) {
    new Slider(".js-slider-wrapper", ".js-slide-navigation");
} else {
    $(window).off();
    $(".js-slide-navigation").on("click", SimpleNav);
    let first = false;
    let intersectionObserver = new IntersectionObserver(function () {
        if (first) {
            $(".js-menu-open").toggleClass("blue");
        }
        first = true;
    });
    // start observing
    intersectionObserver.observe(document.querySelector(".page-2__right"));

}

function SimpleNav(e) {
    let node = $(e.target);
    let toSlide = node.data("scroll");
    if (toSlide !== undefined) {
        MenuModule.menuClose();
        $("html, body").animate({
            scrollTop: $(`#${toSlide}`).offset().top
        });
    }
}


function isTouchDevice() {
    try {
        document.createEvent("TouchEvent");
        return true;
    }
    catch (e) {
        return false;
    }
}

let _window = window;
$(window).resize(function () {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function () {
        if (_window.outerWidth > 1024) {
            new Slider(".js-slider-wrapper", ".js-slide-navigation");
        } else {
            $(window).off("wheel");
        }
    }, 500);
});



