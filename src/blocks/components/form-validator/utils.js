function checkEmpty(node) {
	return !!(node.is("input") && node.val() === "");
}

function checkMain(node) {
	let patternMail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
	return !patternMail.test(node.val());
}

export {
	checkEmpty,
	checkMain
};