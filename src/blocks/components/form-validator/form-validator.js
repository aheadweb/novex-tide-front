import $ from "jquery";
import { checkEmpty, checkMain } from "%components%/form-validator/utils";

class formValidator {
    constructor(form, obj) {
        this.form = form;
        this.option = obj;
        this.errors = {
            empty: "Пустое поле",
            phone: "Неверно заполненный телефон",
            mail: "Неправильный email",
            default: "Дефолтная ошибка хакер"
        };
        this.formValid = true;

    }

    valid() {
        return this.form === null || this.formValid;
    }

    parseForm() {
        this.formValid = true;
        $(this.form).find("input").each((number, input) => {
            let node = $(input);
            if (node.data("valid") === undefined) {
                return;
            }
            let whichValidator = node.data("valid").split(" ");
            this.parseNodeByValidators(whichValidator, node);
        });

    }

    parseNodeByValidators(arrayValidators, node) {
        let lengthValidators = arrayValidators.length;
        let indexValidator = 0;
        let recursionCheck = () => {
            if (indexValidator < lengthValidators) {
                let errorResult = this.checkNodeByValidatorType(arrayValidators[indexValidator], node);
                if (errorResult.error) {
                    this.setErrorOnInput(node, errorResult.msg);
                    this.formValid = false;
                    return;
                } else {
                    this.removeErrorOnInput(node);
                }
                indexValidator++;
                recursionCheck();
            }
        };
        recursionCheck();
    }


    setErrorOnInput(node, error) {
        node.removeClass("success");
        node.addClass("error");
        node.closest(".js-label").find(".msg-error").text(this.errors[error]);
    }

    removeErrorOnInput(node) {
        node.removeClass("error");
        node.addClass("success");
        node.closest(".js-label").find(".msg-error").text("");
    }



    checkNodeByValidatorType(validatorType, node) {
        let error = "";
        let hasError = false;
        switch (validatorType) {
        case "empty":
            error = checkEmpty(node) ? validatorType : "";
            break;
        case "mail":
            error = checkMain(node) ? validatorType : "";
            break;
        default:
            error = "default";
            break;
        }

        if (error) {
            hasError = true;
        }

        return {
            error: hasError,
            msg: error,
        };
    }
}


$(".js-checked").on("change", ({target}) => {
    let btn = $(".js-send-form");
    target.checked 
        ? btn.attr("disabled",false)
        : btn.attr("disabled",true);

});

$(".js-send-form").on("click", () => {
    let validator = new formValidator(".js-form");
    validator.parseForm();
    console.log(validator.valid());
});
